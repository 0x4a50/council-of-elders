## The Council of Elders ##

A model of governance for communal co-living

## Glossary ##

There are a number of formal terms used throughout this document, which will be _italicized_ when used.  These terms have a specific meaning as outlined below:

- **Member** - someone who has been accepted as a part of the community, may be a _resident_, and may be given a _position_ and one or more _roles_.  Members are required to adhere to the _laws_ and _values_ of the community as determined and enforced by the _council_.  
- **Resident** - a _member_ who lives on land owned or rented by one of the _owners_.  
- **Position** - a _member_ can have exactly one position, which grants certain powers and responsibilities and may only be determined by vote of the _council_.  
- **Elder** - a _position_ for one of the governing _members_ of the community, responsible for creating and enforcing _laws_ within the community and assigning _roles_ to _members_.  
- **Owner** - a _position_ filled by anyone whose name is on the lease or holds a deed for the land or dwellings that the community _members_ use.  Has the same powers and responsibilities as an _elder_ but cannot be removed from the _council_ and is legally responsible for community spaces and their use.  
- **Council** - the collection of _elders_ and _owners_ who collectively edit and enforce the _laws_ and _values_ of the community.  They meet regularly to discuss items on the _agenda_.  Any description of the activities of the _council_ necessarily involves the direct participation of each member of the counsel.  
- **Role** - a _member_ should be given at least one role to promote participation in and smooth function of the community.  Roles involve some responsibility and failure to complete those responsibilities may be punished by the council.  These roles are defined by the _council_ in the Roles document.  
- **Values** - all _members_ are expected to exemplify the community values in their day-to-day conduct.  Although there are no specific consequences for not abiding by these values, failure to do so by an individual _member_ may influence decisions by the _council_ about that _member_.  _Values_ are defined by the _council_ in the Values document.  
- **Laws** - rules that all community _members_ must abide by.  Laws are defined by the _council_ in the Laws document, and the _council_ votes on the consequences for specific instances of infraction.  Laws are intended to be minimal and designed to decrease friction within the community workings.  Laws should only be made for _important_ issues, that cannot or should not be easily resolved by ordinary discussion between _members_.  The _council_ is responsible for interpreting and enforcing the laws.  Punishments for lawbreaking may include: imposed behaviors, fines, removal of role, removal of resident status (with appropriate notice), removal of membership.     
- **Important** - a class of problem that is not easily resolved by the involved parties in casual conversation or by simple temporary votes, or a problem that consistently recurs or requires a large amount of time to resolve or necessitates coordination between many members.  A problem must be important before it can be added to the _agenda_ for the _council_ to discuss.  If the _council_ decides the issue is not important, that item on the _agenda_ will be disregarded.  
- **Agenda** - the list of action items the _council_ will vote on at their next meeting.  Any _member_ may add to this list but only _council_ meetings can remove an item from this list.

## Forming a Community ##

The community is formed when three or more people agree to become _members_ and also either _elders_ or _owners_ for that community and the appropriate information is entered on the Council document.  Once the council is formed and the community is given a name, they can proceed to fulfill their duties in editing the Values, Laws, and Roles documents, bringing in new _members_, assigning _roles_, and so on.

## Council Meetings ##

On at least a monthly interval, the entire _council_ will meet either in person or remotely to deal with items on the _agenda_.  _Members_ may add items to the _agenda_ at will anytime before the start of the meeting.  _Agenda_ items and the resulting decisions of the _council_ will be publicly available for community members.  The items on the _agenda_ will be handled in chronological order from the first entry to the last.  For each item on the _agenda_ the following will occur:

1. The relevant parties will be given a brief opportunity to discuss the item and any reasons for it.  
2. The _council_ will decide if the item is _important,_ and if not the item will be dismissed and the next item will begin.  Otherwise, if the item is _important_, the council will dismiss all outside observers and continue to the next steps.  
3. The _council_ will discuss the situation at hand, and vote on one of the following: 
    - create, edit, or remove a _law_ from the Laws document, or decide on a punishment for a particular instance of infraction of the law  
    - create, edit, or remove a _value_ from the Values document  
    - assign or remove a _role_ from a _member_  
    - assign or remove a _position_ from a _member_  
    - add or remove _resident_ status from a member  
    - add or remove a person from the _members_ list  
4. The item will be marked with the relevant decision and the next item on the _agenda_ will take the focus of the meeting.  

## Voting ##

Voting within the _council_ follows a simply majority vote with at least 51% agreement necessary to pass.  This holds for all decisions of the _council_ unless explicitly stated in the special votes section.  Votes will be structured as a declaration of action followed by a yes or no decision on which to vote, where each _council_ member gets one vote.  Any member of the _council_ may make a declaration of action on which to vote.  Not all _council_ members must vote, but a simple majority will result in the declared action being carried out.

### Special Votes ###

Certain votes are more potent and require more stringent voting rules.  These can be one of the following:

- **Unanimous vote** - all members must vote and must vote "yes" in order for the declaration to pass.  A law with particularly extreme consequences like eviction can be designated to first require a unanimous vote by the _council_.  Adding an _elder_ to the council may be done with a unanimous vote.  
- **Council Restructuring vote** - to add or remove the position of a _member_ without their consent a particularly stringent vote must take place.  This is expected to be rarely if ever used, because it represents a serious conflict amongst the leadership.  Nevertheless it may be necessary.  A council restructuring vote requires a unanimous vote from all members of the _council_ except the member in question.  It also requires the vote of five _members_ chosen from the community by a standard vote from the council.  If all of these votes are affirmative, then a member of the _council_ may be removed without their consent.  This is not possible in the case of _owners_, who must be a member of the _council_ at all times.  
- **Act of War vote** - the _council_ may, by a unanimous vote, declare a state of war in the community and elect a warlord with unilateral power to perform or delegate all functions of the community.  The elected warlord remains in power until removed by a simple majority vote of the _council_, and until that time the council will have no power make any other decisions for the community.  This action is intended never to be used.  It exists only for extreme emergencies in which the act of voting by multiple people would be too slow to manage the needs of the community and only when the community members are in a persistent state of mortal peril.  

## Delegating with Roles ##

In addition to the duties above, the _council_ is responsible for the well-being of the community and its _members_ and are the first point of contact for any _important_ decisions in the community.  This may represent a large burden, which can be mitigated by delegating these responsibilities to members with _roles_ that empower them to engage in these tasks.  For example, a common _role_ might be a groundskeeper, whose responsibility would be to ensure the maintenance and overall integrity of property the community uses.  Another expected _role_ might be a community coordinator who handles events and ensures guests come and go without incident.